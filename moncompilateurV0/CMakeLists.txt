cmake_minimum_required(VERSION 3.21)
project(moncompilateurv2)

set(CMAKE_CXX_STANDARD 14)

add_executable(moncompilateurv2
        compilateur.cpp)
