//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"

#include<map>
#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <ctype.h>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {STRING,INT,FLOAT,BOOLEAN,CHAR};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, enum TYPE> DeclaredVariables;
//set<string> DeclaredVariables;
unsigned long long TagNumber=0;

bool IsDeclared(const char *id){
	return (DeclaredVariables.find(id)!=DeclaredVariables.end());
	//use map to save the declared varibles and their types
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
enum TYPE Identifier(void){
	enum TYPE type;
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

enum TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return(INT);
}

enum TYPE Expression(void);			// Called by Term() and calls Term()

enum TYPE Factor(void){
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
		return Identifier();
	}
	else 
	{
		enum TYPE a;
		if (current==NUMBER)
		{
			 a = Number();
		}
		else
		{
			if(current==ID)
				Identifier();
			else
				Error("'(' ou chiffre ou lettre attendue");
		}
		return a;
	}
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPE Term(void){
	OPMUL mulop;
	enum TYPE typeFactor1,typeFactor2;
	typeFactor1 = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		typeFactor2 = Factor();
		if(typeFactor1 == typeFactor2)
		{
			//Factor();
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(mulop){
				case AND:
					if(typeFactor2 !=BOOLEAN)
						Error("le type doit être BOOLEAN dans l'expression");
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND"<<endl;	// store result
					break;
				case MUL:
					if(typeFactor2 !=INT)
						Error("le type doit être INT dans l'expression");
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
					break;
				case DIV:
					if(typeFactor2 !=INT)
						Error("le type doit être INT dans l'expression");
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
					break;
				case MOD:
					if(typeFactor2 !=INT)
						Error("le type doit être INT dans l'expression");
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD"<<endl;		// store result
					break;
				default:
					Error("opérateur multiplicatif attendu");
			}
		}
		else
		{
			string var = (const char*)typeFactor1;
			Error("type dans Factor"+ var +"attendu");
		}
	}
	return typeFactor1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPE SimpleExpression(void){
	OPADD adop;
	enum TYPE typeSExpression1,typeSExpression2;
	typeSExpression1 = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		typeSExpression2 = Term();
		if(typeSExpression1 == typeSExpression2)
		{
		//Term();
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(adop){
				case OR:
					if(typeSExpression2 !=BOOLEAN)
						Error("le type doit être BOOLEAN dans l'expression");
					cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
					break;			
				case ADD:
					if(typeSExpression2 !=INT)
						Error("le type doit être INT dans l'expression");
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					break;			
				case SUB:	
					if(typeSExpression2 !=INT)
						Error("le type doit être INT dans l'expression");
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
					break;
				default:
					Error("opérateur additif inconnu");
			}
			cout << "\tpush %rax"<<endl;			// store result
		}
		else
		{
			string var = (const char*) typeSExpression1;
			Error("type dans Therm"+var+"attendu");
		}
	}
	return typeSExpression1;

}

// DeclarationPart := "[" Ident {"," Ident} "]"
/*
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}
*/
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}



// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPE Expression(void){
	OPREL oprel;
	enum TYPE typeExpression1,typeExpression2;
	unsigned long long tag;
	typeExpression1 = SimpleExpression();
	if(current==RELOP){
		tag = ++TagNumber;
		oprel=RelationalOperator();
		typeExpression2 = SimpleExpression();
		if(typeExpression1 == typeExpression2)
		{
			//SimpleExpression();
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
			switch(oprel){
				case EQU:
					cout << "\tje Vrai"<<tag<<"\t# If equal"<<endl;
					break;
				case DIFF:
					cout << "\tjne Vrai"<<tag<<"\t# If different"<<endl;
					break;
				case SUPE:
					cout << "\tjae Vrai"<<tag<<"\t# If above or equal"<<endl;
					break;
				case INFE:
					cout << "\tjbe Vrai"<<tag<<"\t# If below or equal"<<endl;
					break;
				case INF:
					cout << "\tjb Vrai"<<tag<<"\t# If below"<<endl;
					break;
				case SUP:
					cout << "\tja Vrai"<<tag<<"\t# If above"<<endl;
					break;
				default:
					Error("Opérateur de comparaison inconnu");
			}
			cout << "\tpush $0\t\t# False"<<endl;
			cout << "\tjmp Suite"<<tag<<endl;
			cout << "Vrai"<<tag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
			cout << "Suite"<<tag<<":"<<endl;
		}
		else
		{
			string var = (const char*)typeExpression1;
			Error("type dans Expression"+var+"attendu");
			return BOOLEAN;
		}
	}
	return typeExpression1;
}

void DisplayStatement(void){
	enum TYPE displayType;
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	if(!IsDeclared(lexer->YYText()))
		Error("Indent non declaré");
	displayType=DeclaredVariables[lexer->YYText()];
	switch(displayType)
	{
	case INT:
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
		break;
	case BOOLEAN:
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;
			break;
	case FLOAT:
			cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
			cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
			break;
	default:
			Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
		}

	current=(TOKEN) lexer->yylex();

}


// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	enum TYPE typeA1,typeA2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	typeA1 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	typeA2 = Expression();
	if(typeA2 != typeA1)
	{
		cerr<<"Type variable "<<typeA1<<endl;
		cerr<<"Type Expression "<<typeA2<<endl;
		Error("types incompatibles dans l'affectation");
	}
	if(typeA1== CHAR)
	{
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else
		cout << "\tpop "<<variable<<endl;
	
}

void Statement(void);

//IfStatement:= "IF" Expression "THEN" Statement ["ELSE" Statement]
void IfStatement(void)
{
	current = (TOKEN) lexer->yylex();
	unsigned long long tag = ++TagNumber;
	if(Expression() == BOOLEAN)
	{
		cout << "\t pop %rax \t"<<endl;
		cout<<"\t cmpq $0, %rax \t"<<endl;
		cout<<"\t je ELSE \t"<<tag<<"\t# si faux jump au ELSE"<<tag<<endl;
		if( current != CONDITION || strcmp (lexer->YYText(), "THEN")!=0)
		{
			Error("not cle THEN attendu");
		}
    	current = (TOKEN) lexer->yylex();
		Statement();
		cout<<"\t jmp NEXT"<<tag<<"\t # permet de sauter le else"<<endl;
		cout<<"\t ELSE"<<tag<<":"<<endl;
		if(current != CONDITION || strcmp (lexer->YYText(),"ELSE")==0)
		{
			current = (TOKEN) lexer->yylex();
			Statement();
		}
		cout<<"NEXT"<<tag<<":"<<endl;
	}
	else
	{
		Error(" expression n'est' pas de type BOOLEAN");
	}
}	
//WhileStatement:= "WHILE" Expression "DO" Statement
void WhileStatement(void)
{
	unsigned long long tag = ++TagNumber;
	cout<<"WHILE"<<tag<<":"<<endl;
    current = (TOKEN) lexer->yylex();
	if( Expression() == BOOLEAN)
	{
		cout<<"\t pop %rax \t"<<endl;
		cout<<" \t cmpq $0, %rax"<<endl;
		cout<<"\t je ENDWHILE"<<tag<<"\t # saute au end du while "<<tag<<endl;
    	if(current != CONDITION || strcmp(lexer->YYText(), "DO")!=0)
    	{
    	    Error("not cle DO attendu");
    	}
    	else
    	{
    	    current = (TOKEN) lexer->yylex();
    	    Statement();
			cout<<"\t jmp WHILE"<<tag<<endl;
			cout<<"ENDWHILE"<<tag<<":"<<endl;
    	}
	}
	else
	{
		Error(" expression n'est' pas de type BOOLEAN");
	}
}
//ForStatement:= "FOR" AssignementStatement "TO"|"DOWNTO" INT "DO" SimpleExpression | Statement
void ForStatement(void)
{
	unsigned long long tag = ++TagNumber;
	string var = " ";
	current = (TOKEN) lexer->yylex();
	if(current!=ID )
          Error("Identificateur attendu");

	if(!IsDeclared(lexer->YYText())!=0){

	  Error("variable non declaree");	
          exit(-1);
        }
	
	var=lexer->YYText(); 

	cout<<"FOR"<<tag<<":"<<endl;
   
    AssignementStatement();
	cout << "\t movq $0, %rax"<<endl;
	//current = (TOKEN) lexer->yylex();
    /*if(current != CONDITION )
        Error("not cle TO ou DOWNTO attendu");*/
	if(strcmp(lexer->YYText(), "TO")==0)
	{
		current = (TOKEN) lexer->yylex();
    	
		cout<<"\t push "<<var<<endl; //recupere la valeur de expression
		cout<<"\tpop %rax"<<endl;
		cout<<"\tpop %rcx"<<endl;
		cout<<"\t cmpq $rcx, %rax"<<endl;
		cout<<"\t ja ENDFOR"<<tag<<"\t # si faux on saute au ENDFOR "<<tag<<endl;
		current = (TOKEN) lexer->yylex();
        if(current != CONDITION || strcmp(lexer->YYText(), "DO")!=0)
        	Error("not cle DO attendu");
        current = (TOKEN) lexer->yylex();
		while(current != CONDITION)
		{
    		SimpleExpression();
			current = (TOKEN) lexer->yylex();
		}
		while(current == CONDITION)
		{
    		Statement();
			current = (TOKEN) lexer->yylex();
		}
		cout << "\tmovq" << var << "%rax" << endl;
		cout << "\taddq $1, %rax" << endl;
		cout << "\tmovq %rax," << var << endl;
		cout<< "\tpush %rax"<<endl;
		cout<<"\t jmp FOR"<<tag<<"\t"<<endl;

		cout<<"ENDFOR"<<tag<<":"<<endl;
		
    	
	}
	if(strcmp(lexer->YYText(), "DOWNTO")!=0)
	{
		current = (TOKEN) lexer->yylex();
    	if(DeclaredVariables[lexer->YYText()]==INT)
		{
			cout<<"\t push "<<var<<endl; //recupere la valeur de expression
			cout<<"\tpop %rax"<<endl;
			cout<<"\tpop %rcx"<<endl;
			cout<<"\t cmpq $rcx, %rax"<<endl;
			cout<<"\t ja ENDFOR"<<tag<<"\t # si faux on saute au ENDFOR "<<tag<<endl;
			current = (TOKEN) lexer->yylex();
        	if(current != CONDITION || strcmp(lexer->YYText(), "DO")!=0)
        		Error("not cle DO attendu");
        	current = (TOKEN) lexer->yylex();
			while(current != CONDITION)
			{
    			SimpleExpression();
				current = (TOKEN) lexer->yylex();
			}
			while(current == CONDITION)
			{
    			Statement();
				current = (TOKEN) lexer->yylex();
			}
			cout << "\taddq $1, %rax" << endl;
			cout << "\tmovq %rax," << var << endl;
			cout<< "\tpush %rax"<<endl;
			cout<<"\t jmp FOR"<<tag<<"\t"<<endl;
			cout<<"ENDFOR"<<tag<<":"<<endl;
    	}
	}
    

}
void CaseListElement();
void CaseLabelList();
void Empty();
//<case statement> ::= case <expression> of <case list element> {; <case list element> } end
void CaseStatement(void)
{
	current = (TOKEN) lexer->yylex();
	Expression();
	current = (TOKEN) lexer->yylex();
	if(strcmp(lexer->YYText(), "OF")!=0)
		Error("mot cle OF attendu");
	current = (TOKEN) lexer->yylex();
	CaseListElement();
	current = (TOKEN) lexer->yylex();
	while(current == COMMA)
	{
		CaseListElement();
		current = (TOKEN) lexer->yylex();
	}
	if (current != CONDITION || strcmp(lexer->YYText(),"END")!=0)
	{
		Error("mot cle END attendu");
	}
	current = (TOKEN)lexer->yylex();
}

//case list element> ::= <case label list> : <statement> | <empty>
void CaseListElement(void)
{
	CaseLabelList();
	current = (TOKEN) lexer->yylex();
	if (current != COLON || strcmp(lexer->YYText(),":")!=0)
		Error(": attendu");
	current = (TOKEN) lexer->yylex();
	if(strcmp(lexer->YYText(),"")!=0)
		Statement();
	Empty();
}

//<case label list> ::= <constant> {, <constant> }
void CaseLabelList(void)
{
	
}

//<empty>::=
void Empty(void)
{

}

//BlockStatement:= "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void)
{
    current = (TOKEN)lexer->yylex();
    Statement();
    while(current == SEMICOLON )
    {
        current = (TOKEN)lexer->yylex();
        Statement();

    }
    if (current != CONDITION || strcmp(lexer->YYText(),"END")!=0)
    {
        Error("not cle END attendu");
    }
    current = (TOKEN)lexer->yylex();
}

enum TYPE Type(void){
	if(current!=TYPE)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INT")==0){
		current=(TOKEN) lexer->yylex();
		return INT;
	}
	else if(strcmp(lexer->YYText(),"FLOAT")==0){
		current=(TOKEN) lexer->yylex();
		return FLOAT;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else
		Error("type inconnu");
	
}

//VarDeclaration := Ident {"," Ident}":" TYPE 
void VarDeclaration(void)
{
	set<string> vars;
	enum TYPE typeVar;
	if(current!=ID)
		Error("Identificateur attendu");
	vars.insert(lexer->YYText());
	current=(TOKEN)lexer->yylex();
	while (current == COMMA)
	{
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Identificateur attendu");
		vars.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current != COLON)
		Error(" : attendu");
	current = (TOKEN)lexer->yylex();
	typeVar = Type();

	for (set<string>::iterator i=vars.begin(); i!=vars.end(); ++i)
	{
	    cout << *i << ":\t.quad 0"<<endl;
            DeclaredVariables[*i]=typeVar;
	}
		
	//store the declared variable and their type in the map declared above
}
//VarDeclarationPart := "VAR" VarDeclaration{";" VarDeclaration}"."
void VarDeclarationPart(void)
{
	current = (TOKEN)lexer->yylex();
	VarDeclaration();
	while(current == SEMICOLON)
	{
		current = (TOKEN)lexer->yylex();
		VarDeclaration();
	}
	if(current != DOT || strcmp(lexer->YYText(),".")!=0)
	{
		Error(" DOT attendu");
	}
	current = (TOKEN)lexer->yylex();
}

// Statement := AssignementStatement
void Statement(void){
	
	if(current  == CONDITION)
	{
		if( strcmp (lexer->YYText(), "IF")==0)
		{
			IfStatement();
		}
        if( strcmp (lexer->YYText(), "WHILE")==0)
        {
            WhileStatement();
        }
        if( strcmp (lexer->YYText(), "FOR")==0)
        {
            ForStatement();
        }
        if( strcmp (lexer->YYText(), "BEGIN")==0)
        {
            BlockStatement();
        }
		if( strcmp (lexer->YYText(), "DISPLAY")==0)
        {
            DisplayStatement();
        }
		
	}
	else
		AssignementStatement();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==VAR && strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





